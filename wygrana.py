import pygame
def wygrana(name, stan_konta):
    pygame.init()
    window = pygame.display.set_mode((800, 600))
    pygame.display.set_caption("Koło fortuny")
    background = pygame.image.load("background_wynik1.png")
    font = pygame.font.Font(None, 50)

    def wpisywanie_do_rankingu(name, stan_konta, plik):
        with open(plik, "a") as f:
            f.write(f'"{name}":{stan_konta},')

    wpisywanie_do_rankingu(name, stan_konta, "ranking.txt")

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

        window.blit(background, (0, 0))
        stan_wygranej = font.render(str(stan_konta), True, (0, 0, 0))
        window.blit(stan_wygranej, (350, 300))

        pygame.display.update()

