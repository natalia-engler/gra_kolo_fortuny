def ranking(plik):
    with open(plik, 'r') as f:
        zawartosc = f.read()
        zawartosc = zawartosc[:-1]  #Usuwa ostatni przecinek
        pary = zawartosc.split(",")  #Rozdziela wpisy
        lista_slownikow = []

        for para in pary:
            name, value = para.split(":")
            name = name.strip().strip('"')
            value = int(value)  #Zamiana

            nowa_para = {name: value}
            lista_slownikow.append(nowa_para)

        print(lista_slownikow)

    lista_imion=[]
    lista_wygranych=[]
    for i in lista_slownikow:
        for key,value in i.items():
            lista_imion.append(key)
            lista_wygranych.append(value)

    def bubble_sort(lista1, lista2):
        n = len(lista1)
        for k in range(1,n):
            for l in range(1,n):
                if lista1[l] > lista1[l-1]:
                    lista1[l], lista1[l-1] = lista1[l-1], lista1[l]
                    lista2[l], lista2[l-1] = lista2[l-1], lista2[l]
        return lista1, lista2

    bubble_sort(lista_wygranych,lista_imion)

    top_5_imion=lista_imion[0:5]
    top_5_wynikow=lista_wygranych[0:5]

    return top_5_imion,top_5_wynikow
"""
    def bubble_sort(slownik):
        i=1
        lista_imion=[]
        lista_wygranych=[]
        for keys,values in slownik.items():
            lista_imion += keys
            lista_wygranych += values
        return lista_imion,lista_wygranych

    print(bubble_sort)
"""


"""
    with open(plik, 'w') as g:
        nowa = "{" + zawartosc + "}"
        g.write(nowa)
"""

ranking("ranking.txt")
