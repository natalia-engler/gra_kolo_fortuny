import pygame
from ranking import ranking

def topka():
    pygame.init()
    window = pygame.display.set_mode((800, 600))
    pygame.display.set_caption("Koło fortuny")
    background = pygame.image.load("background_topka.png")
    font = pygame.font.Font(None, 50)

    lista_list = ranking("ranking.txt")
    dlugosc = len(lista_list[0])

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

        window.blit(background, (0, 0))

        if dlugosc >= 1:
            top1a = font.render(str(lista_list[0][0]), True, (0, 0, 0))
            top1b = font.render(str(lista_list[1][0]), True, (0, 0, 0))
            window.blit(top1a, (250, 200))
            window.blit(top1b, (450, 200))

        if dlugosc >= 2:
            top2a = font.render(str(lista_list[0][1]), True, (0, 0, 0))
            top2b = font.render(str(lista_list[1][1]), True, (0, 0, 0))
            window.blit(top2a, (250, 250))
            window.blit(top2b, (450, 250))

        if dlugosc >= 3:
            top3a = font.render(str(lista_list[0][2]), True, (0, 0, 0))
            top3b = font.render(str(lista_list[1][2]), True, (0, 0, 0))
            window.blit(top3a, (250, 300))
            window.blit(top3b, (450, 300))

        if dlugosc >= 4:
            top4a = font.render(str(lista_list[0][3]), True, (0, 0, 0))
            top4b = font.render(str(lista_list[1][3]), True, (0, 0, 0))
            window.blit(top4a, (250, 350))
            window.blit(top4b, (450, 350))

        if dlugosc == 5:
            top5a = font.render(str(lista_list[0][4]), True, (0, 0, 0))
            top5b = font.render(str(lista_list[1][4]), True, (0, 0, 0))
            window.blit(top5a, (250, 400))
            window.blit(top5b, (450, 400))

        pygame.display.update()

