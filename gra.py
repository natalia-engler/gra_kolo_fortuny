import pygame
import random
import sys
from klasy import kolo, nazwa_kategorii, Text
from wygrana import wygrana

def gra(imie):
    global stan_konta

    def krecenie(lista):
        global kolo_fortuny
        i = random.randint(1, 15)
        j = i % 8
        licznik_zakrecen = 0

        while licznik_zakrecen < i:
            kolo_fortuny = kolo(250, 300, j)
            licznik_zakrecen += 1
            j = (j + 1) % 8
            window.fill((193, 249, 255))
            kolo_fortuny.draw(window)
            kategoria.draw(window)
            stan_hasla = font.render(odgadniete, True, (0, 0, 0))
            window.blit(stan_hasla, (250, 150))
            stan_wygranej = font.render(str(stan_konta), True, (0, 0, 0))
            window.blit(stan_wygranej, (0, 100))
            pygame.display.update()
            clock.tick(10)

        if licznik_zakrecen == i:
            kolo_fortuny = kolo(250, 300, j)
            return lista[j]

    def zgadywanie(litera, stan_odgadnietego_hasla, haslo, licznik_bledow):
        global stan_konta
        litera = litera.upper()

        if len(litera) == 1 and litera.isalpha():
            nowy_stan = ""
            dodaj_punkt = False

            for i in range(len(haslo)):
                if haslo[i].upper() == litera:
                    nowy_stan += litera
                    dodaj_punkt = True
                else:
                    nowy_stan += stan_odgadnietego_hasla[i]

            if dodaj_punkt:
                stan_konta += krecenie(nagrody)

            stan_odgadnietego_hasla = nowy_stan

        elif litera == haslo:
            stan_odgadnietego_hasla = haslo
            stan_konta += krecenie(nagrody)
        else:
            licznik_bledow += 1

        if stan_odgadnietego_hasla == haslo:
            wygrana(imie, stan_konta)
        else:
            return stan_odgadnietego_hasla

    pygame.init()

    window = pygame.display.set_mode((800, 600))
    pygame.display.set_caption("Koło fortuny")
    window.fill((193, 249, 255))

    nagrody = (500, 250, 600, 100, 750, 1000, 50, 25) #jakby nie działało to zmienić na listę

    hasla0 = ["ROBERT LEWANDOWSKI", "IGA ŚWIĄTEK", "CRISTIANO RONALDO", "USAIN BOLT", "BARTOSZ KUREK",
              "AGNIESZKA RADWAŃSKA", "LIONEL MESSI", "SERENA WILLIAMS", "SIMONE BILES", "MARIUSZ PUDZIANOWSKI"]

    hasla1 = ["PIŁKA NOŻNA", "LEKKOATLETYKA", "SZERMIERKA", "SIATKÓWKA", "TENIS", "PŁYWANIE DŁUGODYSTANSOWE",
              "WSPINACZKA GÓRSKA", "KOSZYKÓWKA", "ŁUCZNICTWO", "FUTBOL AMERYKAŃSKI"]

    hasla2 = ["BIEŻNIA", "RAKIETA TENISOWA", "KIJ BASEBALLOWY", "KULA DO KRĘGLI", "ORBITEREK", "HANTLE", "OSZCZEP",
              "NARTY", "SZTANGA", "KIJ HOKEJOWY"]

    numer_kategorii = random.randint(0, 2)

    if numer_kategorii == 0:
        kategoria = nazwa_kategorii(5, 0, "0")
        haslo_do_zgadniecia = hasla0[random.randint(0, 9)]
    elif numer_kategorii == 1:
        kategoria = nazwa_kategorii(5, 0, "1")
        haslo_do_zgadniecia = hasla1[random.randint(0, 9)]
    elif numer_kategorii == 2:
        kategoria = nazwa_kategorii(5, 0, "2")
        haslo_do_zgadniecia = hasla2[random.randint(0, 9)]

    font = pygame.font.Font(None, 50)

    odgadniete = ""
    for znak in haslo_do_zgadniecia:
        if znak == " ":
            odgadniete += " "
        else:
            odgadniete += "-"

    kolo_fortuny = kolo(250, 300, 0)

    stan_konta = 0

    clock = pygame.time.Clock()

    litera = Text(208, 200, 370, 40)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    odgadniete = zgadywanie(litera.text.upper(), odgadniete, haslo_do_zgadniecia.upper(), 0)
                    litera.text = ""

                elif event.key == pygame.K_BACKSPACE:
                    litera.text = litera.text[:-1]

                elif event.key == pygame.K_SPACE:
                    litera.text += " "

                elif event.unicode.isalnum():
                    litera.text += event.unicode

        window.fill((193, 249, 255))
        kategoria.draw(window)
        litera.draw(window)
        kolo_fortuny.draw(window)

        stan_hasla = font.render(odgadniete, True, (0, 0, 0))
        window.blit(stan_hasla, (250, 150))

        stan_wygranej = font.render(str(stan_konta), True, (0, 0, 0))
        window.blit(stan_wygranej, (0, 100))

        pygame.display.update()
