import pygame
from gra import gra

class Text:
    def __init__(self, x, y, width, height, maxlen=-1):
        self.x_cord = x
        self.y_cord = y
        self.width = width
        self.height = height
        self.font = pygame.font.SysFont("Arial", 24)
        self.text = ""
        self.maxlen = maxlen
        self.active = False

    def click(self, events):
        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    return self.text
                elif event.key == pygame.K_BACKSPACE:
                    self.text = self.text[:-1]
                elif len(self.text) < self.maxlen or self.maxlen == -1:
                    if self.active:
                        self.text += event.unicode


        if pygame.mouse.get_pressed(3)[0]:
            if pygame.rect.Rect(self.x_cord, self.y_cord , self.width, self.height).collidepoint(pygame.mouse.get_pos()):
                self.active = True
            else:
                self.active = False

    def draw(self, win):
        pygame.draw.rect(win, (255, 255, 255), (self.x_cord, self.y_cord, self.width, self.height), border_radius = 20)
        font_image = self.font.render(self.text, True, (0, 0, 0))
        win.blit(font_image, (self.x_cord + 5, self.y_cord + 5))

def name():
    pygame.init()
    window = pygame.display.set_mode((800, 600))
    pygame.display.set_caption("Koło fortuny")
    background = pygame.image.load("background_name.png")
    wpisz_imie = Text(208, 288, 370, 40, 15)
    clock = pygame.time.Clock()
    imie_gracza = ""

    while not imie_gracza:
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

        imie_gracza = wpisz_imie.click(events)

        window.blit(background, (0, 0))
        wpisz_imie.draw(window)
        pygame.display.update()
        clock.tick(30)

    gra(imie_gracza)


