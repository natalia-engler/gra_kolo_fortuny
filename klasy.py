import pygame


class nazwa_kategorii:
    def __init__(self, x_cord, y_cord, file_name):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.kategoria_image = pygame.image.load(f"kategorie/kategoria{file_name}.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.kategoria_image.get_width(),
                                  self.kategoria_image.get_height())

    def draw(self, window):
        window.blit(self.kategoria_image, (self.x_cord, self.y_cord))


class kolo:
    def __init__(self, x_cord, y_cord, file_name):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.kategoria_image = pygame.image.load(f"kolo/kolo{file_name}.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.kategoria_image.get_width(),
                                  self.kategoria_image.get_height())

    def draw(self, window):
        window.blit(self.kategoria_image, (self.x_cord, self.y_cord))

class Text:
    def __init__(self, x, y, width, height):
        self.x_cord = x
        self.y_cord = y
        self.width = width
        self.height = height
        self.font = pygame.font.SysFont("Arial", 24)
        self.text = ""

    def draw(self, win):
        pygame.draw.rect(win, (255, 255, 255), (self.x_cord, self.y_cord, self.width, self.height), border_radius=20)
        font_image = self.font.render(self.text, True, (0, 0, 0))
        win.blit(font_image, (self.x_cord + 5, self.y_cord + 5))