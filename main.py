import pygame
from name import name
from topka import topka
class Button:
    def __init__(self, x_cord, y_cord, file_name):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load(f"buttons/{file_name}1.png")
        self.hovered_button_image = pygame.image.load(f"buttons/{file_name}2.png")
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(), self.button_image.get_height())

    def click(self):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            if pygame.mouse.get_pressed()[0]:
                return True

    def draw(self, window):
        if self.hitbox.collidepoint(pygame.mouse.get_pos()):
            window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))

def menu():
    pygame.init()
    window = pygame.display.set_mode((800, 600))
    pygame.display.set_caption("Koło fortuny")
    background = pygame.image.load("background_menu.png")
    zagraj = Button(100, 400, "zagraj")
    ranking = Button(500, 400, "ranking")

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

        window.blit(background, (0, 0))
        zagraj.draw(window)
        ranking.draw(window)
        pygame.display.update()

        if zagraj.click():
            name()

        if ranking.click():
            topka()

menu()