import pytest
def bubble_sort(lista1, lista2):
    n = len(lista1)
    for k in range(1, n):
        for l in range(1, n):
            if lista1[l] > lista1[l - 1]:
                lista1[l], lista1[l - 1] = lista1[l - 1], lista1[l]
                lista2[l], lista2[l - 1] = lista2[l - 1], lista2[l]
    return lista1, lista2

def test_bubble_sort():
    assert bubble_sort([500,300,600],["Asia","Ola","Paula"]) == ([600,500,300],["Paula","Asia","Ola"])

test_bubble_sort()
